# DP Desktop App

Aplicación de escritorio en Java para el registro y consulta de informacion de distribuidores.

##     ⚡️Pre requisitos ##
1. Contar con la ultima version de [Java](https://www.java.com/es/download/) y el entorno de desarrollo [Apache NetBeans IDE 14](https://netbeans.apache.org/download/nb14/nb14.html)
2. contar con [XAMPP](https://www.apachefriends.org/es/index.html) ya que con esto nos da un servidor y base de datos local

##     Para comenzar ##
1. Clonar este repositorio desde el CMD o git bash posicionandote en cualquier parte del equipo: https://gitlab.com/RicardoAngulo/dpdesktopapp.git
2. Descomprimir carpeta practica_dp.zip e importar archivo .sql en http://localhost:8081/phpmyadmin/
3. Iniciar el proyecto
4. Iniciar la depuración o ejecución del proyecto.

## Correr la aplicación

Para correr la aplicación debes de abrir el proyecto con netbeans y correr el formulario frmDistribuidores

## frmDistribuidores 
```
Graba informacion de un distribuidor
```

## mdlConsultarDistribuidor
```
Consulta informacion de un distribuidor
```

NOTA: En mi equipo local, cambie el puerto 80 por 8081, ya que el puerto 80 lo tengo ocupado con IIS.

## Dudas y comentarios les dejo mi correo:

ricardoangulo_@hotmail.com
