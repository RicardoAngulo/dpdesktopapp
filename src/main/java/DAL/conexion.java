/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author rangulo
 */
public class conexion {

    private Connection jdbcConnection;
    private String jdbcURL = "jdbc:mysql://localhost:3306/practica_dp";
    private String jdbcUsername = "root";
    private String jdbcPassword = "";
    
    public conexion() {
        
    }

    public conexion(String jdbcURL, String jdbcUsername, String jdbcPassword) {
        this.jdbcURL = jdbcURL;
        this.jdbcUsername = jdbcUsername;
        this.jdbcPassword = jdbcPassword;
    }

    public void conectar() throws SQLException {
        if (jdbcConnection == null || jdbcConnection.isClosed()) {
            try {
                Class.forName("com.mysql.jdbc.Driver");
            } catch (ClassNotFoundException e) {
                throw new SQLException(e);
            }
            jdbcConnection = DriverManager.getConnection(
                    jdbcURL, jdbcUsername, jdbcPassword);
        }
    }

    public void desconectar() throws SQLException {
        if (jdbcConnection != null && !jdbcConnection.isClosed()) {
            jdbcConnection.close();
        }
    }

    public Connection getJdbcConnection() {
        return jdbcConnection;
    }
    
    public int ejecutarSentenciaSQL(String sentenciaSQL) throws SQLException {
        try {
            conectar();
            PreparedStatement pstm = jdbcConnection.prepareStatement(sentenciaSQL);
            pstm.execute();
            desconectar();
            return 1;
        } catch(SQLException e) {
            System.out.println(e);
            desconectar();
            return 0;
        }
    }
    
    public ResultSet consultarRegistros(String sentenciaSQL) throws SQLException {
        try {
            conectar();
            PreparedStatement pstm = jdbcConnection.prepareStatement(sentenciaSQL);
            ResultSet respuesta = pstm.executeQuery();
            desconectar();
            return respuesta;
        } catch(SQLException e) {
            System.out.println(e);
            desconectar();
            return null;
        }
    }
}
